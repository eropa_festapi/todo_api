from fastapi import Depends, FastAPI, HTTPException
from databases import Database
from pydantic import BaseModel
from typing import Optional
from passlib.context import CryptContext
from fastapi.security import OAuth2PasswordBearer
from fastapi.middleware.cors import CORSMiddleware

from jose import jwt



pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

app = FastAPI()

origins = [

    "http://localhost",
    "http://localhost:8080",
    "http://127.0.0.1:3000",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="login")
# URL для PostgreSQL (измените его под свою БД)
DATABASE_URL = "postgresql://postgres:postgres@db_todo/postgres"

# Configure JWT settings
SECRET_KEY = "mysecretkey"
ALGORITHM = "HS256"


database = Database(DATABASE_URL)

def hash_pass(password:str):
    return pwd_context.hash(password)

# Модель User для валидации входных данных
class UserCreate(BaseModel):
    username: str
    email: str
    hashed_password : str


class UserLogin(BaseModel):
    email: str
    password : str

class UserTokken(BaseModel):
    tokken: str

# Модель User для валидации исходящих данных - чисто для демонстрации (обычно входная модель шире чем выходная, т.к. на вход мы просим, например, пароль, который обратно не возвращаем, и другое, что не обязательно возвращать)
class UserReturn(BaseModel):
    username: str
    email: str
    id: Optional[int] = None


# тут устанавливаем условия подключения к базе данных и отключения - можно использовать в роутах контекстный менеджер async with Database(...) as db: etc
@app.on_event("startup")
async def startup_database():
    await database.connect()
@app.on_event("shutdown")
async def shutdown_database():
    await database.disconnect()


# Register User
@app.post("/register", response_model=UserReturn)
async def create_user(user: UserCreate):
    query = "INSERT INTO users (name, email, hashed_password) VALUES (:username, :email, :hashed_password) RETURNING id"
    values = {"username": user.username, "email": user.email, "hashed_password": hash_pass(user.hashed_password)}
    try:
        user_id = await database.execute(query=query, values=values)
        return {**user.dict(), "id": user_id}
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed to create user")


def create_jwt_token(data: dict):
    return jwt.encode(data, SECRET_KEY, algorithm=ALGORITHM)

#Auth login user
@app.post("/login")
async def post_login(user: UserLogin):
    query = "SELECT *  FROM users WHERE email=:email"
    values = { "email": user.email}
    try:
        db_users = await database.fetch_one(query=query, values=values)
        is_verified = pwd_context.verify(user.password, db_users.hashed_password)
        if is_verified:
            ## query = "INSERT INTO tokens (user_id) VALUES (:user_id) RETURNING token "
            ## values = {"user_id": db_users.id }
            return {"token": create_jwt_token({"sub": db_users.email}), "token_type": "bearer"}
        else:
            raise HTTPException(status_code=500)
    except Exception as e:
        raise HTTPException(status_code=500, detail="Failed  login")

# Get user token
async def get_user_from_token(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])  # декодируем токен
        return payload.get("sub")  # тут мы идем в полезную нагрузку JWT-токена и возвращаем утверждение о юзере (subject); обычно там еще можно взять "iss" - issuer/эмитент, или "exp" - expiration time - время 'сгорания' и другое, что мы сами туда кладем
    except jwt.ExpiredSignatureError:
        pass  # тут какая-то логика ошибки истечения срока действия токена
    except jwt.InvalidTokenError:
        pass


#Get data user
def get_user(email: str):
    query = "SELECT *  FROM users WHERE email=:email"
    values = {"email":email}
    db_users = database.fetch_one(query=query, values=values)
   # arStr= [{"id":db_users.id}]
    return db_users

@app.get("/users/me/")
async def read_users_me( current_user: str = Depends(get_user_from_token) ):
    user = await  get_user(current_user)
    if user:
        return user
    return {"error": "User not found"}
