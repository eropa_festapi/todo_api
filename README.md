# Install
Creating a decor using the command
`docker-compose up --b`


execute a command in sql in the database
`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`


We go to the docker
`docker-compose exec app_pythontodo bash`


Go to the app folder
`cd app`


Starting the migration
`alembic upgrade head`


# Run
Let's launch the docker that has already been built
`docker-compose up`


# create migration
We go to the docker
`docker-compose exec app_pythontodo bash`


Creating a migration
`alembic revision --autogenerate -m " name migration "`


We are migrating
`alembic upgrade head`
